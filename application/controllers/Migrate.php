<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migrate for Migration Process
 */
class Migrate extends CI_Controller
{
	/**
	 * Function to migrate to the latest version of migration
	 * @return $messages
	 */
    public function index()
    {
    	$this->load->library('migration');
        $migration_okay = $this->migration->latest();
        $messages = '';
        if (!$migration_okay)
        {
            $messages = $this->migration->error_string();
        }
        else
        {
            $messages = 'Migrated to latest version successfully';
        }
        echo $messages;
    }

    /**
     * @param  $year for desired year of migration
     * @param  $month for desired month of migration
     * @param  $day for desired day of migration
     * @param  $hour for desired hour of migration
     * @param  $minute for desired minute of migration (optional)
     * @param  $second for desired second of migration (optional)
     * @return $messages
     */
    public function version($year, $month, $day, $hour, $minute='', $second='') {
    	$this->load->library('migration');
    	$minute = ($minute != '' ? $minute : '00');
    	$second = ($second != '' ? $second : '00');
    	$version = $year.''.$month.''.$day.''.$hour.''.$minute.''.$second;

    	$status = $this->migration->version($version);
    	$messages = '';
    	if(!$status) {
    		$messages = $this->migration->error_string();
    	}
    	else {
    		$messages = 'Migrated Successfully to Migration Version: '.$version;
    	}

    	echo $messages;
    }

}